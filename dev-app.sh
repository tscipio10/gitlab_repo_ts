#!/bin/bash

# check the date
echo  TIMEDATECTL >> /tmp/serverinfo.info
timedatectl >> /tmp/serverinfo.info

# check last 10 logins
echo LOGINS >> /tmp/serverinfo.info
last | head -n 10 >> /tmp/serverinfo.info

# check SWAP
echo SWAP >> /tmp/serverinfo.info
free -h | grep -i swap >> /tmp/serverinfo.info

# check kernel version
echo KERNEL_VERSION >> /tmp/serverinfo.info
uname -r >> /tmp/serverinfo.info


# check your ip address
echo IP ADDRESS >> /tmp/serverinfo.info
ip a | grep inet >> /tmp/serverinfo.info
